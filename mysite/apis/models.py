from django.db import models
import uuid


class Student(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    mobile_number = models.IntegerField(null=True)

    def __str__(self):
        return self.name + '-' + self.last_name + '-' + str(self.mobile_number)


class Color(models.Model):
    RED = 'RED'

class RandomUser(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    age = models.IntegerField(null=True)
    code = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    def __str__(self):
        return self.name + '-' + self.last_name + '-' + str(
            self.age) + '-' + self.code
