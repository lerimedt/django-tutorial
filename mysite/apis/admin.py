from django.contrib import admin
from apis.models import Student
from apis.models import RandomUser


class B2BConsumerAdmin(admin.ModelAdmin):
    search_fields = ('name', 'last_name',)
    list_display = ['name', 'last_name']


class AuthorRandomUser(admin.ModelAdmin):
    pass

admin.site.register(Student, B2BConsumerAdmin)
admin.site.register(RandomUser, AuthorRandomUser)
