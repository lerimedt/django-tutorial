# Generated by Django 2.1.5 on 2019-01-15 20:34

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0003_color_randomuser'),
    ]

    operations = [
        migrations.AlterField(
            model_name='randomuser',
            name='age',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='randomuser',
            name='code',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
    ]
