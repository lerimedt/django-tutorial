from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from mysite.apis.serializers import StudentSerializer
from mysite.apis.models import Student



class StudentView(APIView):
    def get(self, request, format=None):
        students = Student.objects.all()
        serializer = StudentSerializer(students, many=True)
        return Response(serializer.data)

# Create your views here.
