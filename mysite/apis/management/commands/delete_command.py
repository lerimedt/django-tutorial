from django.core.management.base import BaseCommand, CommandError

from apis.models import Student


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('id', type=str)

    def handle(self, *args, **options):
        id = options.get('id')
        if id:
            student = Student.objects.get(pk=id).delete()
            if not student:
                raise CommandError('Student is not been removed')
        else:
            raise CommandError('Student is not exist')
