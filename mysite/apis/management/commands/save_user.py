from django.core.management.base import BaseCommand, CommandError

from apis.models import RandomUser
import requests
from itertools import islice


class Command(BaseCommand):


    def handle(self, *args, **options):
        url = 'https://randomuser.me/api/?results=5000'
        req = requests.get(url)
        users = req.json().get('results')

        obj_list = []
        batch_size = 500
        for user in users:
            name = user['name']['first']
            last_name = user['name']['last']
            obj_list.append(
                RandomUser.objects.create(name=name, last_name=last_name))
        while True:
            batch = list(islice(obj_list, batch_size))
            if not batch:
                break
            RandomUser.objects.bulk_create(batch, batch_size)


